/**
 * Created by Francis Chia on 7/11/2016.
 */

var express = require("express");
var app = express();
var bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());

app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/bower_components"));


app.post("/register", function(req, res){
    var name = req.body.params.name;
    //var gender = req.body.params.gender;
    var address = req.body.params.address;
    var country = req.body.params.country;
    var email = req.body.params.email;
    var contact = req.body.params.contact;
    var dob = req.body.params.dob;
    var password = req.body.params.password;

    console.info("Name : %s", name);
    console.info("Address: %s", address);
    console.info("Country: %s", country);
    console.info("Email: %s", email) ;
    console.info("Contact: %d", contact) ;
    console.info("Date of Birth: %s", new Date(dob)) ;
    console.info("Password: %s", password) ;

    res.status(200).end();
});

var portNumber = process.argv[2] || 3000;
console.info(process.argv[2]);

app.listen(parseInt(portNumber), function(){
    console.info("Webserver started on port 3000");
});


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


// Load Express
//var express = require("express");

// Create an instance of express
//var app = express();

// Create request processing pipeline
//app.use(express.static(__dirname + "/public"));
//app.use("/bower_components", express.static(__dirname +"/bower_components"));

// Start server

//app.set("port", process.argv[2] || process.env.APP_PORT || 3000);
//app.listen(app.get("port"),function(){
//    console.info("Application started on port %d", app.get("port"));
//    });