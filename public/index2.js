/**
 * Created by Francis Chia on 7/11/2016.
 */

var ValidationApp = angular.module("ValidationApp", []);

(function(){
    var ValidationCtrl;

    ValidationCtrl = function($http){
        var ctrl = this;

        // ng model for name
        ctrl.name = "";
        // ng model for gender
        //ctrl.gender = "";
        // ng model for address
        ctrl.address = "";
        // ng model for country
        ctrl.country = "";
        // ng model for email
        ctrl.email = "";
        // ng model for contact
        ctrl.contact = "";
        // ng model for dob
        ctrl.dob = "";
        // ng model for password
        ctrl.password = "";
        
        // response message
        ctrl.status = {
            message: "",
            code: 0
        };

        ctrl.register = function() {
            $http.post("/register",{
                params : {
                    name : ctrl.name,
                    //gender : ctrl.gender,
                    address  : ctrl.address,
                    country : ctrl.country,
                    email : ctrl.email,
                    contact : ctrl.contact,
                    dob : ctrl.dob,
                    password : ctrl.password
                    
                }
            }).then(function(){
                console.info(" Success");
                ctrl.status.message ="Your registration is complete!";
                ctrl.status.code = 202;
                //$window.Location = "/thankyou";
            }).catch(function(){
                console.info(" Error");
                ctrl.status.message = "Your registration failed!";
                ctrl.status.code = 400;
            });
        };
    };

    ValidationApp.controller("ValidationCtrl", ['$http', ValidationCtrl]);
})();